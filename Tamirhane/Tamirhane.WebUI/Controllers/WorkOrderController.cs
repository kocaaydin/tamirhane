﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tamirhane.Data;

namespace Tamirhane.WebUI.Controllers
{
    public class WorkOrderController : Controller
    {
        private TamirhaneContext db = new TamirhaneContext();

        // GET: WorkOrder
        public ActionResult Index()
        {
            var workOrder = db.WorkOrder.Include(x=>x.Appointment).Include(w => w.CustomerCar).Include(x=>x.Customer);
            return View(workOrder.ToList());
        }

        // GET: WorkOrder/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkOrder workOrder = db.WorkOrder.Find(id);
            if (workOrder == null)
            {
                return HttpNotFound();
            }
            return View(workOrder);
        }

        // GET: WorkOrder/Create
        public ActionResult Create(int AppointmentId = 0)
        {
            var model = new WorkOrder();
            if (AppointmentId>0)
            {
                var app = db.Appointment.Include("CustomerCar").Include("Customer").FirstOrDefault(x => x.Id == AppointmentId);
                model.AppointmentInfo = app.AppointmentDate.ToString() + " " + app.AppointmentDescription;
                model.CustomerCar = app.CustomerCar;
                model.Customer = app.Customer;
                model.CustomerCarId = app.CustomerCar.Id;
                model.CustomerId= app.CustomerCar.CustomerId;

            }
            return View(model);
        }

        // POST: WorkOrder/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(WorkOrder model, int AppointmentId = 0)
        {
            
            if (ModelState.IsValid)
            {
                db.WorkOrder.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            if (AppointmentId > 0)
            {
                var app = db.Appointment.Include("CustomerCar").Include("Customer").FirstOrDefault(x => x.Id == AppointmentId);
                model.AppointmentInfo = app.AppointmentDate.ToString() + " " + app.AppointmentDescription;
                model.CustomerCar = app.CustomerCar;
                model.Customer = app.Customer;
            }
            return View(model);
        }

        // GET: WorkOrder/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkOrder workOrder = db.WorkOrder.Include("Customer").Include("CustomerCar").Include("Appointment").FirstOrDefault(x=>x.Id==id);
            if (workOrder.Appointment != null)
            {
                var app = workOrder.Appointment;
                workOrder.AppointmentInfo = app.AppointmentDate.ToString() + " " + app.AppointmentDescription;
            }
        
            if (workOrder == null)
            {
                return HttpNotFound();
            }
            return View(workOrder);
        }

        // POST: WorkOrder/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( WorkOrder workOrder)
        {
            if (ModelState.IsValid)
            {
                db.Entry(workOrder).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(workOrder);
        }

        // GET: WorkOrder/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkOrder workOrder = db.WorkOrder.Include("CustomerCar").Include("Appointment").FirstOrDefault(x=>x.Id ==id);
            if (workOrder == null)
            {
                return HttpNotFound();
            }
            return View(workOrder);
        }

        // POST: WorkOrder/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            WorkOrder workOrder = db.WorkOrder.Find(id);
            db.WorkOrder.Remove(workOrder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
