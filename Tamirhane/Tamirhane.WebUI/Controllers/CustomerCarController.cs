﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tamirhane.Data;

namespace Tamirhane.WebUI.Controllers
{
    public class CustomerCarController : Controller
    {
        private TamirhaneContext db = new TamirhaneContext();

        public ActionResult CustomerSearchbyPlate(string Plate,int CustomerId)
        {
            var c = db.CustomerCar.Where(x => x.CustomerId == CustomerId && x.Plate.Contains(Plate)).Select(x => new { Plate = x.Plate, Id = x.Id }).OrderBy(x => x.Id).Skip(0).Take(5).ToList();
            return Json(c, JsonRequestBehavior.AllowGet);
        }


        // GET: CustomerCar
        public ActionResult Index(int CustomerId = 0)
        {
            var customerCar = db.CustomerCar.Include(c => c.Customer);
            if (CustomerId>0)
            {
                customerCar = customerCar.Where(x => x.CustomerId == CustomerId);
            }

            return View(customerCar.ToList());
        }

        // GET: CustomerCar/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomerCar customerCar = db.CustomerCar.Find(id);
            if (customerCar == null)
            {
                return HttpNotFound();
            }
            return View(customerCar);
        }

        // GET: CustomerCar/Create
        public ActionResult Create(int CustomerId = 0)
        {
          
            var model = new CustomerCar();
            if (CustomerId>0)
            {
                model.Customer = db.Customer.FirstOrDefault(x => x.Id == CustomerId);
            }

            return View(model);
        }


        // POST: CustomerCar/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Plate,Brand,Model,ModelYear,CustomerId,UpdatedDate,CreatedDate")] CustomerCar customerCar)
        {
            
            if (ModelState.IsValid)
            {
                db.CustomerCar.Add(customerCar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            if (customerCar.CustomerId > 0)
            {
                customerCar.Customer = db.Customer.FirstOrDefault(x => x.Id == customerCar.CustomerId);
            }

            return View(customerCar);
        }

        // GET: CustomerCar/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomerCar customerCar = db.CustomerCar.Include("Customer").FirstOrDefault(x=>x.Id ==id);
            if (customerCar == null)
            {
                return HttpNotFound();
            }

         

            return View(customerCar);
        }

        // POST: CustomerCar/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Plate,Brand,Model,ModelYear,CustomerId,UpdatedDate,CreatedDate")] CustomerCar customerCar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customerCar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            if (customerCar.CustomerId > 0)
            {
                customerCar.Customer = db.Customer.FirstOrDefault(x => x.Id == customerCar.CustomerId);
            }
            string s = "";

            return View(customerCar);
        }

        // GET: CustomerCar/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomerCar customerCar = db.CustomerCar.Find(id);
            if (customerCar == null)
            {
                return HttpNotFound();
            }
            return View(customerCar);
        }

        // POST: CustomerCar/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CustomerCar customerCar = db.CustomerCar.Find(id);
            db.CustomerCar.Remove(customerCar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
