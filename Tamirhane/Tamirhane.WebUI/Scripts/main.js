﻿$(document).ready(function () {
    $("#CustomerAutoComplate").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '/Customer/CustomerSearchbyName?FullName=' + $("#CustomerAutoComplate").val(),
                success: function (data) {
                    response(
                        $.map(data, function (item) {
                            return {
                                label: item.Title,
                                value: item.CustomerId
                            };
                        })
                    );
                },
            });
        },
        select: function (event, ui) {
            $("#CustomerAutoComplate").val(ui.item.label);
            $("#CustomerId").val(ui.item.value);
            return false;
        }
    });


    $("#CustomerCarIdAutoComplate").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '/CustomerCar/CustomerSearchbyPlate?CustomerId=' + $("#CustomerId").val()+'&Plate=' + $("#CustomerCarIdAutoComplate").val(),
                success: function (data) {
                    response(
                        $.map(data, function (item) {
                            return {
                                label: item.Plate,
                                value: item.Id
                            };
                        })
                    );
                },
            });
        },
        select: function (event, ui) {
            $("#CustomerCarIdAutoComplate").val(ui.item.label);
            $("#CustomerCarId").val(ui.item.value);
            return false;
        }
    });

    $('.datepicker').datetimepicker({
        format: 'DD.MM.YYYY HH:mm:ss'
  
    });
});