namespace Tamirhane.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WorkOrder")]
    public partial class WorkOrder
    {
        public int Id { get; set; }

        [Display(Name = "Randevu Bilgileri")]
        public int? AppointmentId { get; set; }

        [StringLength(500)]
        [Required]
        [Display(Name ="�� Emri A��klamas�")]

        public string AppointmentDescription { get; set; }
        [Required]
        [Display(Name = "Plaka")]
        public int? CustomerCarId { get; set; }
        [Required]
        [Display(Name ="M��teri")]
        public int? CustomerId { get; set; }

        public virtual Appointment Appointment { get; set; }
        public virtual Customer Customer { get; set; }

        public virtual CustomerCar CustomerCar { get; set; }

        [NotMapped]
        public string AppointmentInfo { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
