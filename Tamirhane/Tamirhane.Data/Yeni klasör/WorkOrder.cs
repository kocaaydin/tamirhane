namespace Tamirhane.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WorkOrder")]
    public partial class WorkOrder
    {
        public int Id { get; set; }

        public int? AppointmentId { get; set; }

        [StringLength(500)]
        public string AppointmentDescription { get; set; }

        public int? CustomerCarId { get; set; }

        public virtual CustomerCar CustomerCar { get; set; }
    }
}
