namespace Tamirhane.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Appointment")]
    public partial class Appointment
    {
        public int Id { get; set; }

        [Display(Name ="Plaka")]
        [Required]
        public int? CustomerCarId { get; set; }
        [Display(Name = "Randevu Tarihi")]
        [Required]
        public DateTime? AppointmentDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(500)]
        [Display(Name = "Randevu A��klamas�")]
        [Required]
        public string AppointmentDescription { get; set; }
        [Display(Name = "M��teri")]
        [Required]
        public int? CustomerId { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual CustomerCar CustomerCar { get; set; }
    }
}
