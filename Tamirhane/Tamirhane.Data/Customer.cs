namespace Tamirhane.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Customer")]
    public partial class Customer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Customer()
        {
            Appointment = new HashSet<Appointment>();
            CustomerCar = new HashSet<CustomerCar>();
        }

        public int Id { get; set; }

        [StringLength(200)]
        [Required]
        [Display(Name="Ad")]
        public string Name { get; set; }

        [StringLength(200)]

        [Required]
        [Display(Name = "Soyad")]
        public string LastName { get; set; }

        [StringLength(400)]
        public string FullName { get; set; }

        [StringLength(255)]
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [StringLength(50)]
        [Required]
        [Display(Name = "Telefon")]
        public string Phone { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Appointment> Appointment { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerCar> CustomerCar { get; set; }
    }
}
