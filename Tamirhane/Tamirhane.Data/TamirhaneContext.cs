namespace Tamirhane.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TamirhaneContext : DbContext
    {
        public TamirhaneContext()
            : base("name=TamirhaneConnection")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<Appointment> Appointment { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<CustomerCar> CustomerCar { get; set; }
        public virtual DbSet<WorkOrder> WorkOrder { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<CustomerCar>()
                .Property(e => e.Plate)
                .IsUnicode(false);

            modelBuilder.Entity<CustomerCar>()
                .Property(e => e.Brand)
                .IsUnicode(false);

            modelBuilder.Entity<CustomerCar>()
                .Property(e => e.Model)
                .IsUnicode(false);
        }
    }
}
